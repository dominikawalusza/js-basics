//------------------------------------------------------------------
//alert("test"); //lepiej js trzymać w osobnym pliku niż w html

// var x = 2,
//     y = 5;

// alert(x%y);

//------------------------------------------------------------------
//SWITCH z ang. przełącznik

// var dzienTygodnia = 124;

// switch(dzienTygodnia)
// {
//     case 1:
//     case 2:
//     case 3:
//     case 4:
//     case 5:    
//         alert("dzisiaj jest pracujacy dzien");
//         break;
//     case 6:    
//     case 7:        
//         alert("weekend");
//         break;        
//     default:
//         alert("nie ma takiego dnia w tygodniu");
// }

//------------------------------------------------------------------
//  Functions - funkcje - kawałek napisanego kodu, który może być ponownie wykorzystane w wielu miejscach w kodzie bez potrzeby ponownego pisania treści.


// function dodaj(x, y)
// {    
//   return parseInt(x + y);
// }

// function dzielenie(x, y)
// {
//    if(y === 0)
//    {
//        return "Cholero nie dziel przez zero!";
//    }
// //   else nie potrzebne
//    return x / y;
// }

// //var sumaZmiennych = dodaj(2, 7);

// //alert(sumaZmiennych);

// var wynikDzielenia = dzielenie(10, 0);

// alert(wynikDzielenia);

//------------------------------------------------------------------
//Funkcje - zasięg zmiennych - zmienne globalne i lokalne

// var a = 5;

// function test(a) 
// {
//     //var a = 3; //jeśli wypisujemy jako argument to tego nie trzeba
//     a = 3; //zmienna lokalna
//     return a
// }

// alert(test());

// alert(a);

//------------------------------------------------------------------
//Funkcja jako wartość zmiennej / anonimowe funkcje

// var x = function()
// {
//     alert("xxxx");
// };


// x();

// function test(f, y)
// {
//     f(y+10);
// }

// test(
//   function(x){
//    alert("cosik " + x);   
//   },
//   20
// );


// var hi = function(type)
// {
//     if (type === "szef")
//         return function(name){
//           alert("Cześć szefie, " + name + "!");  
//         };
//     else
//         return function(name){
//           alert("Cześć " + name + "!");  
//         };
// };

// var zwroconaFunkcja = hi("szef");

// zwroconaFunkcja("Arek");

//------------------------------------------------------------------
// Obiekty - to pojemniki do przechowywania zmiennych i funkcji tematycznie ze sobą związanych do dalszego łatwiejszego ponownego użycia


// var div = document.getElementById("test"); //pobranie elementu ze strony po id

// div.innerHTML = "nowa treść"; //zmiana treści na stronie

// //tworzenie nowego obiektu
// var osoba = {
//     imie: "Dominika", //właściwości obiektu (zmienne)
//     nazwisko: "Walusza",
//     pobierzInformacje: function() //funkcja anonimowa - metoda
//     {        
//         return this.imie + " " + this.nazwisko;
//     },
//     toString: function() 
//     {
//         return this.imie + " " + this.nazwisko;
//     }
// };

// //alternatywna wersja tworzenia obiektu (zalecana ta wyżej)
// // var osoba = new Object({
// //     imie: "Dominika", 
// //     nazwisko: "Walusza",
// //     pobierzInformacje: function()
// //     {        
// //         return this.imie + " " + this.nazwisko;
// //     },
// //     toString: function()
// //     {
// //         return this.imie + " " + this.nazwisko;
// //     }        
// // }
// // );

// div.innerHTML = osoba; //zmiana treści za pomoca obiektu

// //osoba.imie = "Ewelina"; //imię nie zmieni się (program najpierw wyświetla)

//------------------------------------------------------------------
// Obiekty - to pojemniki do przechowywania zmiennych i funkcji tematycznie 
//          ze sobą związanych do dalszego łatwiejszego ponownego użycia
//          
//Klasa -  W skrócie klasa to forma do wytwarzania obiektów.
//         Ta foremka służy do zebrania obiektów w jedną "klasę". 
//          Daje możliwość stworzenia z tej formy wielu nowych różniących się 
//          minimalnie od siebie obiektów, 
//          ale będących dalej do siebie podobnych cechami i metodami.


// var div = document.getElementById("test");

// //tworzymy klasę (funkcja konstruująca) 
// function osoba(imie, nazwisko, age)
// {
//     this.name = imie;
//     this.surname =  nazwisko;
//     this.age = age;
//     this.toString = function()
//     {        
//         return this.name + " " + this.surname + " " + this.age + " lat";
//     };
// }

// // tworzenie obiektów
// var x = new osoba("Dominika", "Walusza", 26);
// var y = new osoba("Jan", "Nowak", 16);
// var z = new osoba("Mateusz", "Kowalski", 20);

// div.innerHTML = x + "<br>" + y + "<br>" + z;

//------------------------------------------------------------------
//Prototyp - dodawanie nowych właściwości do klas z zewnątrz

// var div = document.getElementById("test");

// function osoba(imie, nazwisko, age)
// {
//     this.name = imie;
//     this.surname =  nazwisko;
//     this.age = age;
//     //this.specifiedValue = 12;
//     this.toString = function()
//     {        
//         return this.name + " " + this.surname + " " + this.age + " lat";
//     };
// }

// osoba.prototype.specifiedValue = 12; //modyfikujemy klasę z zewnątrz
// //x.specifiedValue = 12;

// var x = new osoba("Dominika", "Walusza", 26);
// var y = new osoba("Jan", "Nowak", 16);
// var z = new osoba("Mateusz", "Kowalski", 20);

// div.innerHTML = x.specifiedValue + "<br>" + y.specifiedValue + "<br>" + z.specifiedValue;


//------------------------------------------------------------------
//TABLICE (Arrays - z ang)
// 
// TAB
// ------------------------------------------------
// [0]  |  [1]  |  [2]  |  [3]  |  [4]  |  [5]  |
// ------------------------------------------------
//  
//  

// //tworzymy tablicę
// var produkty = [
//   "PHP",
//   "Mysql",
//   "Javascript"    
// ];

// //dodanie elementu do tablicy
// produkty[3] = "PDO";

// //dodanie elementu do tablicy na końcu
// //produkty.push("PDO");

// //inna metoda tworzenia tablicy
// //var produkty = new Array();

// //wyświetlamy tablicę
// //alert(produkty);

// //ASSOCIATIVE ARRAYS
// //przechowują pary (unikatowy klucz, wartość) i umożliwia dostęp do wartości poprzez podanie klucza. 
// var osoba = [];

// osoba["imie"] = "Arek";
// osoba["nazwisko"] = "Nazwisko";

// //alert(osoba.imie);

// //wyświetlanie treści ze strony
// var kursyProgramowania = document.getElementsByTagName("li");

// alert(kursyProgramowania[6].innerHTML);

//------------------------------------------------------------------
//Operacje na tablicach

// length - zwraca liczbę elementów tablicy
// concat od concatanate (powiązać) łączy / wiąże ze sobą dwie tablice
// join - połączyć elementy tablicy w określony przez nas sposób np. <br>
// pop - usunąć ostatni element tablicy i go zwrócić do dalszej operacji
// push - popchnąć element na koniec tablicy, zwraca długość tablicy
// shift - przesunąć (usunąć pierwszy element z tablicy i go zwrócić)
// unshift - przesuwa wszystko o 1 element i dodaje nowy element
// liczby.sort(function(a, b){ return a - b;}); - sortuje rosnąco, gdy b - a to malejąco, a domyślnie sortuje stringi
// reverse - odwróć kolejność el. w tablicy
// slice - kroić - od (1,3) czyli wyświetla 1 i 2 (bez 3)
// splice - arg1 - od którego el. ma zaczać usuwać, arg2 - il. el ma usunąć - lepić


// var produkty = [
//   "PHP",
//   "Mysql",
//   "Javascript",
//   "PDO",
//   "Grunt.js",
//   "SASS"
// ];

// var liczby = [
// 4, -54, 24, 12, 12, 55  
// ];

// var tmp = ["MySQli", "AJAX"];

// //Przykłady:
// //document.getElementById("rezultat").innerHTML = produkty.length;
// //document.getElementById("rezultat").innerHTML = produkty.concat(tmp);
// //document.getElementById("rezultat").innerHTML = produkty.join("<br>");
// //document.getElementById("rezultat").innerHTML = produkty.pop();
// //document.getElementById("rezultat").innerHTML = produkty.push("nowość");
// //document.getElementById("rezultat").innerHTML = produkty.shift();
// //document.getElementById("rezultat").innerHTML = produkty.unshift("nowsze");
// //document.getElementById("rezultat").innerHTML = produkty.sort();
// //document.getElementById("rezultat").innerHTML = produkty.reverse();
// //document.getElementById("rezultat").innerHTML = liczby.sort(function(a, b){return a - b;});
// //document.getElementById("rezultat").innerHTML = liczby.sort(function(a, b){return b - a;});
// //document.getElementById("rezultat").innerHTML = produkty.slice(0,2);
// document.getElementById("rezultat").innerHTML = produkty.splice(1,2);


// document.getElementById("rezultat").innerHTML += "<br>-----------------------------<br>"; //dla czytelności

// document.getElementById("rezultat").innerHTML += produkty; //zwraca oryginalną tablicę do porównania
// //document.getElementById("rezultat").innerHTML += liczby; 

//------------------------------------------------------------------
// Pętle

// var kursyProgramowania  = [
//   "C++",
//   "Java",
//   "C#",
//   "Pascal"
//   ];

//var rezultat = document.getElementById("rezultat");

// //tworzenie pętli while
// //zmienna początkowa
// var i = 0;
// //pętla while wypisująca wszystkie kursy z tablicy kursyProgramowania
// while(i < kursyProgramowania.length)
// {
//     rezultat.innerHTML += kursyProgramowania[i] + "<br>";

//     i++;
// }

//pobieranie danych ze strony po id 
// var kursyProgramowania = document.getElementById("kursyProgramowania").getElementsByTagName("li");

// var i = 0;
// //dodanie napisu HIT/PROMOCJA
// while(i < kursyProgramowania.length)
// {
//     if (kursyProgramowania[i].innerHTML === "C++")
//         kursyProgramowania[i].innerHTML += " HIT";
//     else
//         kursyProgramowania[i].innerHTML += " PROMOCJA";

//     i++;
// }

//Wykonuje zadane polecenia dopóki warunek jest spełniony. Polecenia wykonywane są przynajmniej raz
// do
// {
//     alert(i);
//     i++;
// }while(i < 5);

//for (TO_CO_MA_BYC_ZAINICJALIZOWANE_NA_SAMYM_STARCIE; WARUNEK_KTORY_MUSI_BYC_SPELNIONY_ABY_PETLA_DALEJ_SIE_WYKONYWALA ; CO_MA_SIE_WYKONAC_PO_PRZEJSCIU_WSZYSTKICH_INSTRUKCJI_W_PETLI

//pętla for kumuluje się w jednej linii
// for (var i = 0; i < 5; i++)
// {
//     document.getElementById("rezultat").innerHTML += i + "<br>";
// }

// break i continue

// var kursyProgramowania = document.getElementById("kursyProgramowania").getElementsByTagName("li");

// // for (var i = 0; i < kursyProgramowania.length; i++)
// // {    

// //     if (i % 2 !== 0)
// //         kursyProgramowania[i].innerHTML = "parzysty: " + kursyProgramowania[i].innerHTML;
// //     else
// //         continue; //przerwij wykonywanie danej iteracji i kontynuuj, następne instrukcje po continue nie zostaną wywołane
// //     //alert(i)

// // }

// for (var i = 0; i < 6; i++)
// {
//     if (i === 4) //tworzymy warunek
//         break; //konczy calkowicie pętle

//     alert(i);
// }

//pętla for / in - wykorzystuje indeks, stosuje się gdy chce się przejść po obiektach w tablicy

// var kursyProgramowania = document.getElementById("kursyProgramowania").getElementsByTagName("li");

// for (var property in kursyProgramowania)
// {
//   if (typeof(kursyProgramowania[property]) !== "object") //szuka obiektów
//        break;

//   alert(kursyProgramowania[property].innerHTML); //wyświetla właściwość obiektu
// }

//---------------------------------------------------------------------------------------------
// arguments Object- jest zmienną lokalną dostępną wewnątrz każdej funkcji (argumenty funkcji)


// function addNumbers() //tworzymy funkcję (można na dwa sposoby z for i for in)
// {
//     var suma = 0; 

//     //pierwszy sposob
//     // for (i = 0; i < arguments.length; i++)
//     // {
//     //     suma += arguments[i];
//     // }

//    //alert(typeof(arguments[0])); // sprawdzanie typu argumentu 0

//     //drugi sposob
//     for (var property in arguments)
//     {
//         suma += arguments[property];
//     }

//     return suma; //zwracamy sumę
// }
// var sum = addNumbers(3,4,7,6,6,4,124,124,124); //wywołujemy funkcję o podanych parametrach

// alert(sum); //wyświetlamy wynik

//------------------------------------------------
//DOM - Document Object Model
//Sposób reprezentacji złożonych dokumentów XML i HTML w postaci modelu obiektowego. Model ten jest niezależny od platformy i języka programowania. 


// nodeName	nazwa węzła (najczęściej nazwa tagu)
// nodeValue	wartosć węzła
// parentNode	rodzic węzła
// childNodes	tablica dzieci danego obiektu
// firstChild	pierwsze dziecko (węzeł)
// lastChild	ostatnie dziecko (węzeł)
// previousSibling	zwraca poprzedni węzeł na tym samym poziomie (jego krewniaka)
// nextSibling         zwraca następny węzeł na tym samym poziomie (jego krewniaka)
// attributes          tablica atrybutów elementu 
//                     attributes[indeks].nodeValue zwraca wartość atrybutu 
//                     lepiej stosować funkcję getAttribute("nazwa")
// textContent zawartość tekstowa JEST WSPIERANY OD IE 9 >
// innerHTML   zawartość HTML - np. do podmiany tagów

// setAttribute("nazwaAtrybutu", "wartosc atrybutu"); //dodanie atrybutu z zewnątrz na stronie i edycja w css
// removeAttribute("nazwaAtrybutudousuniecia"); //usunięcie atrybutu z zewnątrz na stronie (przydatne przy akcjach na stronie)



// var kursyProgramowania = document.getElementById("kursyProgramowania");

// kursyProgramowania.setAttribute("class", "changecolor");
// kursyProgramowania.removeAttribute("class"); 

// //alert(kursyProgramowania.textContent)
// //alert(kursyProgramowania.childNodes[1].innerHTML); //wraz z tagami
// //alert(kursyProgramowania.childNodes[1].textContent); //bez tagów
// //alert(kursyProgramowania.childNodes[1].nextSibling.nodeValue = "new"); 

// var tmp = kursyProgramowania.childNodes[1].parentNode.getAttribute("id");
// alert(tmp);

//-------------------------------------------------------------------------------
//QUERYSELECTOR 

// getElementById()
// getElementsByTagName()

// słabiej wspierane:
// getElementsByClassName() - brak wsparcia w ie6,7,8
// getElementsByName() - brak wsparcia w ie6,7,8,9

// brak wsparcia w ie 6 i 7 i połowiczne w ie8:
// querySelector() - wybranie pierwszego napotkanego elementu spełniającego warunek
// querySelectorAll() - wybieranie wszystkich elementów spełniających warunek

// var kursyProgramowania = document.querySelector("#kursyProgramowania li:nth-child(3)"); //selector z css

// alert(kursyProgramowania.innerHTML);

//-----------------------------------
//Zmiana style za pomocą JS
//Typy dostępne pod adresem: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference

// var kursyProgramowania = document.querySelectorAll("#kursyProgramowania li");

// for(var i = 0; i < kursyProgramowania.length; i++)
// {
//     kursyProgramowania[i].style.display = "none"; //ukrycie
// }

//-----------------------------------------------------------------------------
//Dodawanie i usuwanie elementów znajdujących się na stronie - nie przez innerHTML!

//     createElement("nazwa_tagu"); //dodawanie np. paragrafu do style.css
//     appendChild
//     removeChild
//     tmp.parentNode.removeChild(tmp);

// var x = document.createElement("p");

// x.style.color = "red";
// x.className = "tesciur";
// x.innerHTML = "<p id='testowy2'>nowy tekst<p> 4";

// var body = document.querySelector("body");

// var newChildNode = body.appendChild(x);

// var testowy2 = document.getElementById("testowy2");

// testowy2.style.color = "green";


// var kursyProgramowania = document.getElementById("kursyProgramowania");

// kursyProgramowania.parentNode.removeChild(kursyProgramowania); //usuwa całkowicie, lepiej ukryć w style.css

//----------------------------------------------------------------------------------------
//Pętla w pętli - tabliczka mnożenia (ćwiczenie)

// var tabliczkaMnozenia = "<table>"; // otwieramy tabele

// for (var i = 1; i <= 10; i++)
// {
//    tabliczkaMnozenia += "<tr>";  // tworzymy wiersze

//    for (var j = 1; j <= 10; j++) 
//         tabliczkaMnozenia += "<td>" + i * j + "</td>"; // otiweramy i zamykamy kolumny

//    tabliczkaMnozenia += "</tr>"; // zamykamy wiersze
// }

// tabliczkaMnozenia += "</table>"; // zamykamy tabele

// var rezultat = document.getElementById("rezultat"); // pobieramy element - rezultat ze strony

// rezultat.innerHTML = tabliczkaMnozenia; // podmieniamy element na stronie na nasz stworzony wyżej

//---------------------------------------------------------------------------------------------------------
//Events - zdarzenia, coś co się dzieje na stronie, wskazując jaka funkcja 
//ma zostać wywołana przy konkretnym działaniuu użtkownika
// Więcej na -> Event reference (MDN)

//1. Sposób pierwszy (nie zalecany!!!) - tworzenie funkcji w js i odwołanie w index.html
// function wypiszTekst(tekst)
// {
//   alert(tekst + " !!!!!!!!!!!")
// }

// //2. Sposób drugi - odwołujemy się po id do index.html i tworzymy funkcję np. anonimową
// var test = document.getElementById("test")

// test.onclick = function(){ //można tu podać nazwę funkcji wcześniej stworzonej
//   alert("test")
// }

//------------------------------------------------
//this

// function zmienKolor()
// {
//   this.className = "changeColor";
// }
// function zmienKolor2()
// {
//   this.removeAttribute("class");
// }

//var test = document.getElementById("test");

// test.onmouseover = zmienKolor;
// test.onmouseout = zmienKolor2;

//------------
//Ćwiczenie: przyciski do powiększania/pomniejszania tekstu

// window.onload = function () // dodajemy, żeby strona poprawnie się ładowała
// {
//   var test = document.getElementById("test");
//   var powiekszanie = document.getElementById("plus");
//   var zmniejszanie = document.getElementById("minus");

//   powiekszanie.onclick = function powieksz()
//   {
//     test.className = "plus";
//   }
//   zmniejszanie.onclick = function zmniejsz()
//   {
//     test.className = "minus";
//   }
// }

//---------------------------------------------------
// addEventListener - dodawanie zdarzeń
// removeEventListener - usuwanie zdarzeń
//działa od IE9+ (może być attachEvent/detachEvent w zamian za to, by wspierało)

// Tworzymy 3 funkcje
// function powiekszCzcionke()
// {
//   var fontSize = parseInt(window.getComputedStyle(this).fontSize); //rzutujemy do liczby całkowitej
//   this.style.fontSize = (++fontSize) + "px";
// }

// function zmienKolor() //odwołujemy się do klasy ze stylów
// {
//   this.className = "changeColor";
// }

// function zmienKolor2() //usuwamy klasę
// {
//   this.removeAttribute("class");
// }


// window.onload = function() //dla poprawności ładowania
// {
//   var test = document.getElementById("test");
//   var stop = document.getElementById("stop");

//   //zdarzenia po najechaniu i "odjechaniu" myszki
//   test.addEventListener("mouseover", zmienKolor);
//   test.addEventListener("mouseover", powiekszCzcionke);
//   test.addEventListener("mouseout", zmienKolor2);

//   //zdarzenie po kliknięciu w stop powiększaniu usuwa zdarzenie poprzednie
//   stop.addEventListener("click", function()
//   {
//     test.removeEventListener("mouseover", powiekszCzcionke);
//   });
// };

//--------------------------------------
//Przesyłanie obiektu do funkcji
// altKey, ctrlKey, shiftKey - czy podczas wywołania eventu były wciśnięte klawisze alt, ctrl, shift?
// button - które przyciski myszy zostały kliknięte (nie działa w każdym evencie)
// clientX, clientY - w którym miejscu jest mycha względem 0,0
// keyCode - zwraca w postaci liczby jaki klawisz został wciśnięty, String.fromCharCode(e.keyCode) zwróci jaka to wartość
// target.tagName - nazwa tagu elementu który wywołał zdarzenie wspierane w każdej przeglądarce prócz IE 6-8 
//                  użyj srcElement dla IE na starcie można napisać: var srcElement = e.target ? e.target : e.srcElement;


// function wykonaj(e, str)
// {
//   document.getElementById("tmp");

//   tmp.innerHTML = e.clientX + " " + str;

//   var tooltip = document.getElementById("tooltip");

//   tooltip.style.display = "block";

//   tooltip.style.left = e.clientX + 10 + "px";
//   tooltip.style.top = e.clientY + 10 + "px";
// }

// window.onload = function()
// {
//   var test = document.getElementById("test");

//   test.onmousemove = function(e)
//   {
//     wykonaj(e, this.tagName)
//   }
// }

//------------------------------------------------------
//Propagacja - bąbelkowanie (rozmnażanie) - wywoływanie funkcji wyżej
//By temu zapobiec można wpisać np. e.stopPropagation();


// function wykonaj(e, eventObj)
// {
//  var tmp = document.getElementById("tmp");

//  tmp.innerHTML = "źródło eventu: " + e.target.tagName + "<br>event przypisany do tagu: " + eventObj.tagName;
// }

// window.onload = function()
// {
//   var test = document.getElementById("test");
//   var pogrubiony = document.getElementById("pogrubiony");
//   var przycisk = document.getElementById("przycisk");

//   test.onmousemove = function(e)
//   {
//     wykonaj(e, this)
//   }
// }

//---------------------------------------------------
//preventDefault - zapobieganie akcji zdarzeń 
// np. sprawdzenie poprawności mejla przed wysłaniem

// window.onload = function()
// {
//   var email= document.getElementById("email");
//   var submitFromButton = document.querySelector("#newsletter input[type='submit']");

//   //tworzymy funkcję, gdzie po kliknięciu w przycisk Wyślij, tekst nie zostanie od razu wysłany
//   submitFromButton.onclick = function(e)
//   {
//     var tmp = document.getElementById("tmp");
//     e.preventDefault();

//     //sprawdzamy poprawność mejla
//     tmp.innerHTML = email.value;
//     {
//       if (email.value === "db@op.pl"){
//           this.parentNode.submit();
//           alert("Poprawny e-mail");
//         } else
//         {
//           alert("Nieprawidłowy e-mail!");
//         };
//     }
//   };

//   //wyłaczamy context menu po kliknięciu w przycisk wyślij
//   submitFromButton.oncontextmenu = function(e)
//   {
//     if (e.preventDefault)
//       e.preventDefault();
//     else
//       e.returnValue = false;
//   };
// };

//----------------------------------------------------------------
//Onscroll - przycisk przesuwający stronę do góry

// window.onload = function()
// {
//   window.onscroll = function()
//   {
//     var test = document.getElementById("test2"); //dla ułatwienia dodajemy wartość scrollowania
//     var toTopButton = document.getElementById("toTopButton");

//     var yScrollAxis = window.pageYOffset; //dla ułatwienia dodajemy wartość scrollowania

//     if (yScrollAxis > 200)
//       toTopButton.style.display = "block";
//     else
//       toTopButton.style.display = "none"; //nie wyświetla się przy scrollowaniu < 200

//     test.innerHTML = yScrollAxis; //dla ułatwienia dodajemy wartość scrollowania
//   };
//   toTopButton.onclick = function() //funkcja przesuwania strony do góry
//   {
//     window.scrollBy(0, -1 * window.pageYOffset);
//   };
// };

//-------------------------------------------------------------------------------------------------------
//Mouse events - zdarzenia związane z akcjami myszki

// onmouseover - gdy kursor myszy najedzie na element
// onmouseout - gdy kursor myszy opuście element
// onmousemove - gdy kursor myszy "jeździ" po elemencie.
// onclick = gdy element zostanie kliknięty
// ondblclick = gdy element zostanie 2x szybko kliknięty

// onclick to tak naprawdę:

// onmousedown - gdy mamy wciśnięty przycisk myszy
// onmouseup - gdy opuścimy przycisk myszy

//zeby nie przycinało
// function movingImage(e, objToMove)
// {
//     objToMove.style.left = e.clientX - objToMove.width / 2 + "px"; //zeby przesuwać od srodka obiektu
//     objToMove.style.top = e.clientY - objToMove.height / 2 + "px"; //zeby przesuwać od srodka obiektu
// }

// window.onload = function()
// {
//     var wykrzyknik = document.getElementById("wykrzyknik");

//     wykrzyknik.onmousedown = function() //po kliknięciu powoduje: v
//     {
//         var self = this;
//         document.onmousemove = function(e) //ruch myszki
//         {
//             movingImage(e, self);
//         };
//     };

//     wykrzyknik.onmouseup = function() //po odkliknięciu puść obiekt
//     {
//         document.onmousemove = null;
//     };

//     wykrzyknik.ondragstart = function(e) //zapobieganie ruchu w tle (domyślna funckja przeglądarki)
//     {
//        e.preventDefault();  
//     };
// };

//----------------------------------------------------------------------------------------------------------
// //Funkcje wykonywane po czasie: setTimeout & setInterval

// // setTimeout - która wykonuję operację po określonym czasie z ang. time out - koniec czasu 
// // setInterval - która wykonuję operację co interwał (co określony przez nas czas w nieskończoność)

// // clearTimeout - powstrzymuje operację, która ma zostać wykonana
// // clearInterval - j.w tylko interwał

// //var timeOutStoper; //zmienna globalna (nie zalecane)

// //metoda wykorzystujaca setTimeout
// function stopwatch(uchwytStopera, liczba)
// {
//     uchwytStopera.innerHTML = liczba--; //zmniesza o jeden

//     if (liczba < 0) //kiedy liczba osiagnie 0 konczy dzialanie
//         return;
//     timeOutStoper = setTimeout(function()
//     {
//         stopwatch(uchwytStopera, liczba); //parametry funkcji setTimeout
//     }, 1000); //co 1 sekunde
// }
// //metoda wykorzystujaca setInterval
// function stopwatchInterval(uchwytStopera, liczba)
// {
//     var timeIntervalRef = setInterval(function()
//     {        
//         if (--liczba < 0) //kiedy liczba osiągnie 0, zatrzymaj operacje
//         {
//             clearInterval(timeIntervalRef);
//             return;
//         }

//         uchwytStopera.innerHTML = liczba; //liczba = 0
//     }, 1000);

//     return timeIntervalRef; //zwraca wartosc 
// }
// window.onload = function()
// {
//     var przyciskOdpalStoper = document.getElementById("przyciskOdpalStoper");
//     var przyciskZatrzymajStoper = document.getElementById("przyciskZatrzymajStoper");


//     var uchwytStopera = document.getElementById("uchwytStopera");

//     var timeIntervalRef;

//     przyciskOdpalStoper.onclick = function() //funkcja klikniecia w przycisk wlacz
//     {
//        var poczatkowaWartosc = document.getElementById("poczatkowaWartosc").value;        
//        uchwytStopera.innerHTML = poczatkowaWartosc; 
//        timeIntervalRef = stopwatchInterval(uchwytStopera, poczatkowaWartosc);
//     };
//     przyciskZatrzymajStoper.onclick = function()  //funkcja klikniecia w przycisk zatrzymaj
//     {
//         clearInterval(timeIntervalRef); //zatrzymanie dzialania
//     };


//     //wykorzystanie funkcji przy pomocy zmiennej globalnej (nie zalecane)
//     // przyciskOdpalStoper.onclick = function()
//     // {
//     //    var poczatkowaWartosc = document.getElementById("poczatkowaWartosc").value;        
//     //    uchwytStopera.innerHTML = poczatkowaWartosc;

//     //    if (timeOutStoper) //jesli wystapi timeOutStoper zatrzymaj
//     //        clearTimeout(timeOutStoper);

//     //    stopwatch(uchwytStopera, poczatkowaWartosc);


//     // };
//     // przyciskZatrzymajStoper.onclick = function()
//     // {
//     //     clearTimeout(timeOutStoper);
//     // };


// };

//------------------------------------------------------
//ĆWICZENIE: Stoper z przyciskiem start, stop, reset

// function Stopwatch(uchwytStopera) //klasa stoper
// {
//     this.uchwytStopera = uchwytStopera;
//     this.poczatkowaWartosc;
//     this.timeOutRef = undefined;
//     this.start = function(poczatkowaWartosc)
//     {
//         this.poczatkowaWartosc = poczatkowaWartosc;
//         if (this.timeOutRef)
//             this.stop();

//         this.startStoper();
//     };
//     this.startStoper = function()
//     {
//         if (this.poczatkowaWartosc < 0)
//             return;

//         this.uchwytStopera.innerHTML = this.poczatkowaWartosc--;

//         var self = this;

//         this.timeOutRef = setTimeout(function()
//         {
//             self.startStoper();
//         }, 1000);
//     };
//     this.stop = function()
//     {
//         clearTimeout(this.timeOutRef);
//     };
//     this.reset = function()
//     {
//         if (poczatkowaWartosc)
//         {
//             var clearValue = '';
//             poczatkowaWartosc.value = clearValue;
//             this.start(clearValue);
//         };

//     };
// }



// window.onload = function()
// {
//     var przyciskStart = document.getElementById("przyciskStart");
//     var przyciskStop = document.getElementById("przyciskStop");
//     var przyciskReset = document.getElementById("przyciskReset");


//     var uchwytStopera = document.getElementById("uchwytStopera");

//     var stoper = new Stopwatch(uchwytStopera);

//     przyciskStart.onclick = function()
//     {
//         var poczatkowaWartosc = document.getElementById("poczatkowaWartosc").value; 
//         stoper.start(poczatkowaWartosc);
//     };
//     przyciskStop.onclick = function()
//     {
//         stoper.stop();
//     };
//     przyciskReset.onclick = function()
//     {
//         stoper.reset();
//     };
// };

//----------------------------------------------------------------------------------------
//WALIDACJA, sprawdzanie poprawności danych w formularzach

//    onkeydown - gdy wciskamy jakikolwiek klawisz
//    onkeypress - gdy wciskamy klawisz znakowy (1,2,3,4,a,b,c,d,', / etc.)
//    onkeyup - gdy póścimy jakikolwiek klawisz

//    onchange - gdy user zmieni zawartość i zmieni input
//    onfocus - gdy user 'wejdzie' do inputa
//    onblur - gdy stracimy focus (skupienie) - nie musi się nic zmienić

//    e.which - kod klawisza........... znak = e.which || e.keycode by wspierać ie6-8

//    charCode - kod znaku (tylko niektóre eventy) - nie używać - ma zostać usunięte   

// function isNumber(valueToCheck) //funkcja ułatwiająca sprawdzanie, czy coś jest liczbą
// {
//     return !isNaN(valueToCheck); //czyli jest liczbą
// }

// window.onload = function()
// {
//     var poleLiczbowe = document.getElementById("myForm").poleLiczbowe; //.elements[0]
//     var poleTekstowe = document.getElementById("myForm").poleTekstowe; //.elements[1]

//     var submitMyForm = document.getElementById("myForm").submitMyForm; //.elements[2]
//     //elements używamy np. kiedy chcemy coś dodać w pętli, tak wystarczy odwoływać się po name

//     var info = document.getElementById("info"); //pobieramy element info
//     var i = 0;
//     var isEverythingOK = true;

//     poleLiczbowe.onkeyup = function(e) //kiedy coś wpiszemy i puścimy klawisz
//     {           
//         if (isNumber(this.value)) //kiedy jest liczbą 
//         {
//             this.style.backgroundColor = "green"; //podświetlamy tło pola liczbowego (this) na zielono
//             info.innerHTML = ""; //nic nie wyświetlamy 
//             isEverythingOK = true;
//         }
//         else
//         {
//             e.preventDefault(); //zapobiega
//             this.style.backgroundColor = "red"; //podświetla pole liczbowe (this) na czerwono
//             info.innerHTML = "Niepoprawny format - pole przyjmuje tylko liczby"; //wyświetla komunikat
//             isEverythingOK = false;
//         }       
//     };

//     submitMyForm.onclick = function(e) //funkcja ostatecznie sprawdzająca czy dane są ok
//     {
//         if (!isEverythingOK)
//             e.preventDefault();    //jeśli nie to zapobiega wysłaniu 
//     };
// };

//------------------------------------------------------------------------------------
//checkboxes and radiobutton

// window.onload = function ()
// {
//     var myForm = document.getElementById("myForm"); //pobieramy formularz
//     var submitButton = document.getElementById("myForm").submitButton; //pobieramy przycisk Dodaj

//     var info = document.getElementById("info"); //pobieramy pole komunikatu

//     submitButton.onclick = function(e) //po kliknięciu w przycisk Dodaj
//     {   
//         var tmpString = ""; //tworzymy pustą zmienną by przyspieszyć działanie
//         for (var i = 0; i < myForm.nazwaKursu.length; i++) //pętla po każdym elemencie z formularza
//         {
//             if (myForm.nazwaKursu[i].checked) //jeśli jest zaznaczony
//                 tmpString += myForm.nazwaKursu[i].value + " "; //wypisz wartość ze spacją
//         }
//         info.innerHTML += tmpString + "<br>"; //wypisuje wartości i po każdym dodaj jest "eneter"
//         e.preventDefault(); //jeśli nic nie zaznaczone

//     };
//     for (var i = 0; i <myForm.akceptacjaRegulaminu.length; i++) //sprawdzamy tablicę akceptacjaRegulaminu
//     {
//         myForm.akceptacjaRegulaminu[i].onclick = function() //po kliknięciu w dowolny element wywołuje funkcję
//         { 
//             submitButton.disabled = this.value ==="false"; //jeśli value z akceptacjaRegulaminu jest false to button jest disabled
//         }; //porównanie dotyczy stringa
//     }

// };

//------------------------------------------------
//OPTIONS - rozwijana lista
// .options - tablica z możliwymi opcjami
// .length - ilość elementów w combo box
// .text - zawartość opcji tekstowa - można ją zmienić

// .value; - w evence onchange wartość opcji w atr. value lub tekstowa jeśli value brak

// .selectedIndex - indeks zaznaczonego elementu
// .add - funkcja dodająca nową opcję np.        var newOption = document.createElement("option");  newOption.text = "hej";
// .remove(indeks) - funkcja usuwająca element o indeksie

// window.onload = function()
// {
//     var myFrom = document.getElementById("myForm");
//     var info = document.getElementById("info");
//     var kursy = myFrom.kursy;

//     var newOption = document.createElement("option");
//     newOption.text = "PDO";
//     kursy.add(newOption);
//     kursy.remove(1);

//     kursy.onchange = function()
//     {
//         info.innerHTML = "Wybrano " + kursy.options[kursy.selectedIndex].value + " spośród " + kursy.length + " kursów.";

//     };
// };

//----------------------------------------------------------------
//STRINGI

//Manipulowanie ciągiem znaków

//    substring/slice - potnij od / do
//    substr - tnie od / ILE MA POCIAC
//    split - rozdzielić na tablicę string
//    join - łączy tablicę elmentów w string
//    replace - podmiana
//    trim - usunięcie białych znaków z lewj i prawej strony stringa
//    lastIndexOf - ostatni indeks elementu w stringu
//    indexOf - pierwszy indeks elementu w stringu
//    search 
//    [index] - zamiast charAt(index)

// window.onload = function()
// {
//     var info = document.getElementById("info");
//     info.innerHTML = "Testowy string do testowania \n\
//     leci sobie dalej";

//    var tmp = "dOmInIkA";

//    tmp = tmp.charAt(0).toUpperCase() + tmp.slice(1).toLowerCase();

//    var link = "http://szajer-widawska.walusza.com";

//    var tmpString = "   XHTML PHP MySQL Javascript   ";

// //    var tmpArray = tmpString.split(' ');
// //    tmpArray[1] = "PDO";
// //    var resultString = tmpArray.join(', ');

//    info.innerHTML = tmp; //tmpString.replace('PHP', 'PDO').trim();
// };

//-----------------------------------------------------

// REGEXP
// regular expression - regularne wyrażenia

// stringDoPrzeszukania.search(wzór) - szuka i zwraca indeks
// stringDoPrzeszukania.match(wzór) - szuka i zwraca w postaci tablicy
// regExp.exec(stringDoPrzeszukania) - to samo co wyżej tyle, że nie działa flaga global i zwracana jest tylko jedna wartość
// stringDoPrzeszukania.replace(wzór, "naCO"); - podmienia
// wzór.test(stringDoPrzeszukania); - sprawdza, czy po prostu coś takiego się znajduje w stringu


// g - global - po całym stringu
// i - insensitive (nieczuły na wielkość znaków)

//Jakie wzory(wyrażenia) moze przyjmować RegExp? Oto znaki, które występują w nim (po pauzie co symbolizują):

// -----------------------------------------------------------------------------
// . - dowolny znak
// np. wzór /sm.k/ - znajdzie smak, smok etc.
// np. wzór /A..k/ - znajdzie Arek, etc.
// -----------------------------------------------------------------------------
// * - oznacza, że poprzedzający go znak może wystąpić 0 razy lub wiele razy, a znaczek 'i' od insensitive (nieczuły na wielkość znaków)
// np. wzór /M*arek/i - znajdzie Arek, arek, Marek, marek, mmarek,MMarek MMarek, MmMmmmmmarek ...
// -----------------------------------------------------------------------------
// + - oznacza, że poprzedzający go znak może wystąpić 1 raz lub więcej razy
// wniosek: musi wystąpić przynajmniej raz.
// np. wzór /M+arek/i - znajdzie Marek, marek, mmmmMMarek.
// -----------------------------------------------------------------------------
// ? - oznacza, że poprzedzający znak może wystąpić 1 raz lub 0 razy
// wniosek: znak nie może się powtarzać, ale nie musi się pojawić.
// np. wzór /M?arek/i - znajdzie Arek, arek, Marek, marek.
// -----------------------------------------------------------------------------
// {n} - znajduje dokładnie n powtórzeń poprzedzającego go znaku 
// np. wzór /Zo{2}/ - znajdzie nam tylko Zoo.
// -----------------------------------------------------------------------------
// {n,} - znajduje conajmniej n powtorzen poprzedzajacego go znaku
// np. wzór /Zo{2,}/ - znajdzie nam Zoo, Zooooo, Zoooooooo
// -----------------------------------------------------------------------------
// {n,m} - znajduje minimalnie n potwórzeń, maksymalnie m potwórzeń
// np. wzór /Zo{2,4}/ - znajdzie tylko Zoo, Zooo, Zoooo
// -----------------------------------------------------------------------------
// ^ - upewnia się, że są to pierwsze znaki z całego stringa
// np. wzór /^za/ - znajdzie tylko "za" z zawał, zaklinacz, ale nie znajdzie 
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// [ao] - to co znajduje się pomiędzy kwadratowymi nawiasami, 
// czyli "ao" jest poszukiwane jako pojedyńczy znak.
// np. wzór /sm[ao]k/ - znajdzie smok, smak, ale nie znajdzie smgk, smhk etc.

// [ąśłóżźćĄŚŁÓŻŹĆ]+ - polskie znaki więcej niż jedno
// -----------------------------------------------------------------------------
// [^ao] - to co znajduje sie pomiedzy kwadratowymi nawiasami,
// czyli "ao" jest wykluczone z poszukiwania
// np. wzór /sm[^ao]k/ - znajdzie wszystko prócz smak i smok.
// -----------------------------------------------------------------------------
// [a-d] - w danym miejscu szukany jest zakres liter od a do d, 
// czyli a, b, c, d 
// np. wzór /przykl[a-d]/ - znajdzie przykla, przyklb, przyklc, przykld.
// Wniosek: [0-9] - l. calkowite to samo robi /przykl\d/
// 		 [a-z] - alfabet 
// 		 /w - znajduje jakikolwiek znak alfanumeryczny to samo co [A-Za-z0-9_]
// -----------------------------------------------------------------------------
// [^a-d] - w danym miejscu szukany jest zakres liter nie należących od a do d.
// np. wzor /przykl[^a-d]/ - nie znajdzie tylko przykla, przyklb, przyklc, przykld.
// -----------------------------------------------------------------------------
// \ - pozwala nam interpretować jakichś znak dosłownie np. mając znak *, 
// który oznacza, że chcemy wybrać poprzedzający go znak 0 lub więcej razy, możemy "wyłączyć"
// jego działanie za pomocą \ (backslash)
// np.
//     wzór /K\*\*\*/ znajdzie nam K***
// -----------------------------------------------------------------------------
// \s - znajduje biały znak
// -----------------------------------------------------------------------------
// (x) - oznacza, to że zapamiętuje x;
// np. 
// var d = "AlaArkadiusz";
// var e = d.replace(/(A)(l)(a)/gi, "$3$2$1");
// da nam w zmiennej e: alAArkadiusz
// -----------------------------------------------------------------------------
// x(?=y) - znajduje x pod warunkiem ze zaraz po nim jest y
// var a = "Artur Wlodarczyk, Arkadiusz Kowalski, Arkadiusz Nowak";
// var b = a.replace(/Artur.(?=Wlodarczyk)/gi, "");
// -----------------------------------------------------------------------------
// x(?!y) - odwrotność x(?=y), znajduje x pod warunkem ze po nim nie ma y
// -----------------------------------------------------------------------------
// x|y - znajduje x lub y
// np. wzór /jpg|gif/gi - znajdzie nam w zmiennej b = "kraojobraz.GIF", slowo GIF.
// Wniosek: mozemy sprawdzić czy jakiś plik jest plikiem graficznym.


// window.onload = function()
// {
//    var info = document.getElementById("info");   

//    var indeksy = "A-56 B-12 K-51 A-53 A45 A#56 A.67";

//    var regExp = /A.[0-9]+/g;
//    var row = "";
//    var result = "";

//    while (row = regExp.exec(indeksy))
//    {
//        result += row + " ";
//    }

//    var kodPocztowy = "32-700";

//    var czyKodPocztowy = /[0-9]{2}-?[0-9]{3}/.test(kodPocztowy);

//    info.innerHTML =czyKodPocztowy;


// };


//Regexp - password excercise

// window.onload = function()
// {
//    var info = document.getElementById("info");   

//    var testButton = document.getElementById("myForm").testButton;   

//   // var pw = "aBc4fgąi";

//    testButton.onclick = function(e) //po kliknięciu w button wywołujemy funkcję/ e do preventDeafult
//    {
//        e.preventDefault(); //zapobiega domyślnemu wysłaniu
//        var pw = document.getElementById("myForm").pw.value;   //pobieramy wartość inputa 
//        var regExpPattern = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{7,}/; //musi zawierać cyfrę, mała literę, dużą literę, min. 7 znaków

//        if (regExpPattern.test(pw))//jesli password jest true to
//           document.getElementById("myForm").submit()//zatwierdz formularz
//        else
//           info.innerHTML = "Hasło jest za słabe"; //w innym przypadku wyswietl taki komunikat
//    };
// };

//--------------------------------------------------------------------------------------------------------------

// Manipulacja czasem z pomocą obiektu Date

// getDate()	- dzień miesiąca
// getMonth()	- miesiąc (numerujemy od: 0 - styczeń, 1 - luty etc.)
// getYear()	- zwraca liczbę reprezentującą rok (dla lat 1900 - 1999 jest to 2-cyfrowa liczba 
//                   np. 99, a dla późniejszych jest to liczba 4-cyfrowa np. 2002)
// getFullYear()	- zwraca pełną (full) liczbę reprezentującą rok (np. 1999 lub 2000)
// getHours()	- aktualna godzina
// getMinutes()	- minuty 
// getSeconds()	- sekundy 
// getMilliseconds()- milisekundy 

// getTime()	- aktualny czas w postaci milisekund od godziny 00:00 1 stycznia 1970 roku
// getDay()	- dzień tygodnia (0 dla niedzieli, 1 dla poniedziałku, 2 dla wtorku...)

// toLocaleTimeString() - zwraca część daty związaną z czasem w postaci hh/mm/ss zgodną z normami danego Państwa i 
//                        tego co powie przeglądarka :-)
// toLocaleDateString - zgodnie z normami jak wyżej ale data
// toLocaleString - zgodnie z normami, ale i data i czas

// setTime - ustawia datę na...  w milisekundach od... użycie: var jutro = new Date(dzis.getTime() + 1000*60*60*24); (dodanie 1 dnia)
// setDate - zmienia dzień aktualnego miesiąca na... można wykorzystać np.: dzis.setDate(dzis.getDay() + 3);

// setHours
// setMinutes itd.

// new Date()
// new Date(miliseconds) - ilość milisekund od 1 stycznia 1970 r
// new Date(year, month, day, hours, minutes, seconds, milliseconds) - tylko 3 pierwsze argumenty są wymagane, 
//                                                                     miesiące liczymy od 0
// new Date(dateString) gdzie dateString to data podana w formacie stringu zgodnie z założonymi normami
// np. YYYY-MM-DD, YYYY-MM, YYYY
//     YYYY/MM/DD, MM/DD/YYYY - UWAGA MIESIĄC PODAWAĆ PRZED DNIEM

// Date.prototype.getMonthPL = function() //operacje na klasach
// {
//     var months = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
//                   "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad",
//                   "Grudzień"];

//     return months[this.getMonth()];       
// };

// Date.prototype.getDayPL = function() //operacje na klasach
// {
//     var days = ["Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota","Niedziela"];

//     return days[this.getDay()-1];       
// };
// function ileCzasuMinelo(from, to)
// {
//     return "s:"+(from - to)/1000 + "min:"+(from - to)/1000/60+ "h:"+(from - to)/1000/60/60+ "dni:"+(from - to)/1000/60/60/24;
// }
// function ileCzasuMinie(to)
// {
//     var dzis = new Date();
//     return "Do "+ to.getDate() + "/" + (to.getMonth() + 1) + "/" + to.getFullYear() + " pozostało: " +(dzis - to)/(-1000)+" sekund,"+ " lub: "+(dzis - to)/1000/(-60)+" minut," + " lub: "+(dzis - to)/1000/60/(-60)+" godzin.";
// }


// window.onload = function()
// {
//   var info = document.getElementById("info");   

//   var dzis = new Date();

//   //dzis.setDate(dzis.getDate()+1);

//   //var jutro = new Date(dzis.getTime() + 1000*60*60*24);
//   //var jutro = new Date(dzis.getFullYear(), dzis.getMonth(), dzis.getDate() + 1);
//   var jutro = new Date("2020/06/04");

//   info.innerHTML = this.ileCzasuMinie(jutro) //cwiczenie na podstawie lekcji
//           //dzis.getDayPL();
//           //dzis.getDate() + "/" + (dzis.getMonth() + 1) + "/" + dzis.getFullYear();
// };

//---------------------------------------------------------------------------------------------
// Zegar


// function Clock(elementHandler)
// {
//     this.elementHandler = elementHandler;
//     this.actualDate = new Date();
//     this.start = function()
//     {
//        this.updateElementHandlerContent();
//        var self = this;       
//        setInterval(function(){ self.addSecond(); self.updateElementHandlerContent();}, 1000); //zmienia czas co sekundę
//     };
//     this.addSecond = function()
//     {
//        this.actualDate = new Date();
//     };
//     this.updateElementHandlerContent = function()
//     {
//        this.elementHandler.innerHTML = this.getFormattedDate();  
//     };
//     this.getFormattedDate = function() //edytujemy format zegara
//     {
//        var hours = this.actualDate.getHours();
//        var minutes = this.actualDate.getMinutes();
//        var seconds = this.actualDate.getSeconds();

//        if (hours < 10)
//            hours = "0" + hours;
//        if (minutes < 10)
//            minutes = "0" + minutes;
//        if (seconds < 10)
//            seconds = "0" + seconds;       

//        var suffix = "";
//        if (hours < 12)
//            suffix = "AM";
//        else
//        {
//            hours -= 12;
//            suffix = "PM";
//        }

//        return hours + ":" + minutes + ":" + seconds + " " + suffix;
//     };
// }
// window.onload = function()
// {
//   var info = document.getElementById("info");   
//   var info2 = document.getElementById("info2");  
//   var clock = new Clock(info);
//   var clock2 = new Clock(info2);

//   clock.start();
//   clock2.start();
// };

//------------------------------------------------------
// Cookies - ciasteczka to kontenery przechowujące wartości w postaci:
//             klucz=wartość;klucz2=wartość2;
//           WARTOŚCI TE SĄ DOSTĘPNE DLA NAS NAWET GDY UŻYTKOWNIK OPUŚCI JĄ 
//           przez co możemy identyfikować użytkownika przy ponownej wizycie

//           path=/ 
//           expires= - wygaszenie ważności ciastka domyślicznie po zakończeniu sesji (toUTCString())
//           max-age= - maksymalny wiek cookie podany w sekundach (niewspierany w ie6-8)

// window.onload = function()
// {
//     var info = document.getElementById("info");
//     var stworzCiacha = document.getElementById("stworzCiacha");
//     var usunCiacho = document.getElementById("usunCiacho");

//     info.innerHTML = document.cookie; //wypisuje ciasteczka

//     stworzCiacha.onclick = function() //funkcja tworząca ciasteczka
//     {
//        var expirationDateOfCookie = new Date(); //pobiera datę
//        expirationDateOfCookie.setDate(expirationDateOfCookie.getDate()+30); //ciacho wygasa po 30 dniach

//        document.cookie = "imie=Dominika;max-age="+60*60+";path=/"; //ciacha narzucone, maksymalny czas 60sx60, dostępne na podstronach przez path=/
//        document.cookie = "nazwisko=Baldys;max-age="+60*60+";path=/";
//     };
//     usunCiacho.onclick = function() //funkcja usuwająca ciasteczka
//     {
//        var expirationDateOfCookie = new Date();
//        expirationDateOfCookie.setDate(expirationDateOfCookie.getDate()-1); //wygasa automatycznie po kliknięciu bo odnosi się do przeszłości       
//        document.cookie = "imie=;expires="+expirationDateOfCookie.toUTCString()+";path=/"; 
//     };
// };

// function createCookie(name, value, days) //metoda tworząca ciasteczka
// {
//     var expires = "";
//     if (days) //jeśli podane są dni to po tylu wygasa
//     {
//         var expirationDateOfCookie = new Date();
//         expirationDateOfCookie.setDate(expirationDateOfCookie.getDate()+days);  
//         expires = ";expires=" + expirationDateOfCookie.toUTCString();
//     }

//     document.cookie = name + "=" + encodeURIComponent(value) + expires + ";path=/"; //kodujemy value w przypadku znaków w value po których splitujemy
// }

// function removeCookie(name) //metoda usuwająca ciasteczka
// {
//     var expirationDateOfCookie = new Date();
//     expirationDateOfCookie.setTime(expirationDateOfCookie.getTime()-1);    
//     document.cookie = name+"=;expires="+expirationDateOfCookie.toUTCString()+";path=/";
// }

// function getCookieByName(name) //znajduje ciasteczko po nazwie
// {
//     var cookies = document.cookie.split("; "); //rozdziela stringa po ;  i tworzy tablicę

//     for (var i = 0; i < cookies.length; i++) //przeszukuje tablice
//     {
//         var splittedCookie = cookies[i].split("="); //rozdziela element po =
//         var cookieName = splittedCookie[0]; //wyciąga nazwę

//         if (cookieName === name) //jeśli nazwa jest równa naszej nazwie
//         {
//             var cookieValue = splittedCookie[1]; // to przypisuje value
//             return decodeURIComponent(cookieValue); //zwraca value (już zdekodowane)
//         }
//     }
// }
// window.onload = function()
// {
//     var info = document.getElementById("info"); //dostajemy się do info itd.
//     var stworzCiacha = document.getElementById("stworzCiacha");
//     var usunCiacho = document.getElementById("usunCiacho");

//     info.innerHTML = getCookieByName("imie"); //do info przypisuje wynik z metody z wartości imie

//     stworzCiacha.onclick = function() //tworzymy ciacha
//     {
//        createCookie("imie", "Dominika; sadasa", 30); //sprawdzamy kodowanie URI component
//        createCookie("nazwisko", "Bałdys");       
//     };
//     usunCiacho.onclick = function() //usuwamy ciacha
//     {
//        removeCookie("nazwisko");
//     };
// };

//----------------------------------------------------------------------------------------------------------
//POP UP - wyskakujące okna
//alert - przerywa działanie skyrptu i wyświetla komunikat
//confirm - pyta o potwierdzenie jakiegoś zdarzenia
//prompt - prosi o wypełnienie

// window.onload = function()
// {
//     var deleteAccount = document.getElementById("deleteAccount")
//     var undo = document.getElementById("undo")

//     undo.style.visibility = "hidden" //button undo niewidoczny

//     deleteAccount.onclick = function(){ //funkcja "usuwająca" konto po kliknięciu
//         var name = prompt("Podaj swoje imię"); //pobiera zmienną name
//         var confirmedDelete = confirm(name + " na pewno chcesz usunąć konto?"); //potwierdzenie działania
//         if (confirmedDelete){ //jeśli true
//             alert("Konto zostało usunięte"); //wyświetla alert
//             deleteAccount.style.visibility = "hidden"; //ukrywa opcję usuwania
//             undo.style.visibility = "visible"; //ujawnia opcje cofnięcia zmian
//         };
//     };
//     undo.onclick = function(){ //funkcja przywracająca konto po kliknięciu
//         alert("Konto zostało przywrócone poprawnie") //komunikat z info
//         deleteAccount.style.visibility = "visible"; 
//         undo.style.visibility = "hidden";
//     };
// }

//--------------------------------------------------------------------------------------------------------------------
// Math.round() - zaokrąglenie do całości zgodnie z zasadami matematyki
// Math.ceil() - sufit - zawsze do góry
// Math.floor() - podłoga - zawsze w dół
// toFixed(ileMiejscPoPrzecinku);

// Math.abs() - wartość bezwględna z liczby
// Math.pow(x,y) - x^y

// parseInt - odcina po liczbie całkowitej
// parseFloat("2.567hahaasfasf"); - zachowuje wartości po przecinku (?)
// typeof

// Math.random(); - zwraca losową liczbę od 0 do 1


// window.onload = function()
// {
//     var info = document.getElementById("info");
//     var x = 20.55559;
//     var y = -5;
//     var z = parseFloat("2.1241ks");


//     // info.innerHTML = Math.round(x);
//     // info.innerHTML = Math.ceil(x);
//     // info.innerHTML = Math.floor(x);
//     // info.innerHTML = z.toFixed(2);
//     // info.innerHTML = Math.abs(y);
//     // info.innerHTML = Math.pow(y, 2);
//     // info.innerHTML = parseInt(x);
//     // info.innerHTML = typeof(z);
//     info.innerHTML = Math.random();
// };

//----------------------------------------------
//Random numbers

// window.onload = function()
// {

//     var autor = document.getElementById("autor");
//     var cytat = document.getElementById("cytat");

//     var cytaty = ["Bezpowrotnie stracona jest każda chwila, której nie wypełnia miłość.", //0
//                   "Ludzie myślą czasem jak zabić czas, a to czas ich zabija.", //1
//                   "Tylko życie poświęcone innym warte jest przeżycia.", //2
//                   "Największym złem, na które cierpi świat, jest nie siła złych, lecz słabość dobrych."]; //3

//     var autorzy = ["Torquato Tasso",
//                   "Alphonse Allais",
//                   "Albert Eintstein",
//                   "Monteskiusz"];              

//     var losowaLiczba = Math.floor(Math.random()*4); //losuje liczby od 0 do 3
//     //var losowaLiczba = Math.floor(Math.random()*4+2); //losuje liczby od 2 do 5


//     autor.innerHTML = autorzy[losowaLiczba];
//     cytat.innerHTML = cytaty[losowaLiczba];

// };

//--------------------------------------------------------------------------------------------
// window.location - pobiera adres URL


// window.onload = function()
// {
//     var info = document.getElementById("info");
//     var sample = document.getElementById("sample");

//     sample.onclick = function(e)
//     {
//         e.preventDefault();

//         /*
//          * 
//          * OPERACJE do wykonania przed kliknięciem
//          * 
//          */

//         window.location = this.getAttribute("href");
//     };
//     info.innerHTML = window.location;


// };

//----------------------------------------------------------------
//IMAGES, ładowanie obrazków

// var i = 2; //zaczynamy od drugiego obrazka bo pierwszy już się wyświetla

// function loadMoreImages() //funkcja do buttona
// {
//     if (i < 4) //wyświetlamy do 3 obrazka
//     {
//         var images = document.getElementById("images"); //pobieramy diva w którym będą się wyświetlać obrazki
//         var image = new Image(); //tworzymy nowy obiekt Image

//         image.src = "img/screen"+(i++)+".jpg"; //inkrementacja źródła

//         images.appendChild(image); //dołączamy nowe dziecko czyli obrazek -> szybsze niż inner html

//         if (i !== 4) //nie dodaje buttona jeśli dojdzie do 3 obrazka
//         {
//             var buttonLoadMoreImagesClone = this.cloneNode(true); //klonowanie całego węzła z loadMoreImages wraz z wnętrznem i przypisanie do buttona
//             buttonLoadMoreImagesClone.onclick = loadMoreImages; //sklonowany węzeł (button) po kliknięciu wyświetla obrazek


//             document.body.appendChild(buttonLoadMoreImagesClone); // dołączamy dziecko do body
//         }
//     }
//     this.parentNode.removeChild(this); //musimy usunąć button
// }

// window.onload = function()
// {

//     var loadMoreButton = document.getElementById("loadMoreButton"); //pobieramy button

//     loadMoreButton.onclick = loadMoreImages; //ustawiamy funkcję na kliknięcie

// };

//-------------------------------------------------------------------------------------------
// Slideshow - pokaz slajdów


// window.onload = function()
// {
//     var slideShow = document.getElementById("slideShow"); //pobieramy diva o id slideshow

//     var imagesSrc = ["screen1.jpg", "screen2.jpg", "screen3.jpg"]; //tworzymy tablicę z obrazkami

//     for (var i = 0; i < imagesSrc.length; i++)
//     {
//         var image = new Image(); //tworzymy nowy obiekt image

//         image.src = "img/" + imagesSrc[i]; // podajemy źródlo obrazków do pętli

//         slideShow.appendChild(image); //dołączamy obrazki do slideshow
//     }

//     slideShow.childNodes[0].setAttribute("class", "current"); //ustawiamy klasę dla 1 obrazka na current
//     var i = 0;

//     setInterval(function()
//     {
//         slideShow.childNodes[i % imagesSrc.length].setAttribute("class", ""); //usuwamy klasę current dla modullo z długości tablicy

//         slideShow.childNodes[(i+1) % imagesSrc.length].setAttribute("class", "current"); // dodajemy na nowo dla kolejnego obrazka

//         i++; //inkrementacja

//     }, 3000); //zmiana co 3 sek
// }; 

//-------------------------------------------

// Galeria obrazków z miniaturkami


// window.onload = function()
// {
//     var mainImage = document.getElementById("mainImage");
//     var image = new Image();

//     mainImage.appendChild(image);


//     var thumbnails = document.getElementsByClassName("thumbnail");

//     var currentThumbnail = thumbnails[0];

//     image.src = currentThumbnail.getAttribute("src");
//     currentThumbnail.className += " current";


//     for (var i = 0; i < thumbnails.length; i++)
//     {
//         image.className = " transform";
//         thumbnails[i].onclick = function()
//         {

//             currentThumbnail.className = currentThumbnail.className.replace("current", "");
//             currentThumbnail = this;
//             currentThumbnail.className += " current";
//             image.className = image.className.replace("transform", "");

//             image.src = this.getAttribute("src");
//             image.className += " withoutransform";

//         };



//     }

// };

// $(document).ready(function() {
//     $("#cf7_controls").on('click', 'span', function() {
//         $("#cf7 img").removeClass("opaque");

//         var newImage = $(this).index();

//         $("#cf7 img").eq(newImage).addClass("opaque");

//         $("#cf7_controls span").removeClass("selected");
//         $(this).addClass("selected");
//     });
// });


// TOOLTIP

function createTooltips() { //tworzymy funkcje wyświetlającą tooltipy
    var elementsWithTooltip = document.getElementsByClassName("tooltip"); //pobieramy elementy z klasą tooltip

    var tooltipContainer = document.createElement("div"); //tworzymy element div
    tooltipContainer.id = "tooltipContainer"; //dodajemy adrtybut id do elementu div
    document.body.appendChild(tooltipContainer); //dodajemy do body naszego diva

    var tmpTitles = []; //tymczasowa tabela przechowująca tytuły żeby nie wyświetlaly się domyśle tooltipy

    for (var i = 0; i < elementsWithTooltip.length; i++) { //pętla do elementów z klasą tooltip
        tmpTitles[i] = elementsWithTooltip[i].title; //pobieramy nazwy do tymczasowej tabeli

        elementsWithTooltip[i].tmp_id = i; //nadajemy tymczasowe id 

        elementsWithTooltip[i].addEventListener("mouseover", function(e) { //addEventListener pozwala na przypisanie kilku zdarzeń do jednej funkcji
            tooltipContainer.innerHTML = this.title; //wsadzamy tytuł do kontenera tooltip

            this.title = ""; //"zerujemy" atrybut tytuł
            tooltipContainer.style.left = e.clientX + document.documentElement.scrollLeft + 5 + "px"; //ustawiamy dymek w zależności od myszki
            tooltipContainer.style.top = e.clientY + document.documentElement.scrollTop - 5 + "px";

            tooltipContainer.style.display = "block"; //ustawiamy styl na blokowy
        });
        elementsWithTooltip[i].addEventListener("mouseout", function(e) { //funkcja usuwania dymky po wyjechaniu poza obrazek

            this.title = tmpTitles[this.tmp_id]; //nadajemy tytuł z tymczasowej tabeli
            tooltipContainer.innerHTML = this.title; //wsadzamy tytuł

            tooltipContainer.style.display = "none"; //wyłączamy tooltip
        });
    }
}

window.onload = function() {
    createTooltips();
    var mainImage = document.getElementById("mainImage");
    var image = new Image();

    mainImage.appendChild(image);

    var thumbnails = document.getElementsByClassName("thumbnail");

    var currentThumbnail = thumbnails[0];

    image.src = currentThumbnail.getAttribute("src");
    currentThumbnail.className += " current";

    for (var i = 0; i < thumbnails.length; i++) {
        thumbnails[i].addEventListener("mouseover", function() {

            currentThumbnail.className = currentThumbnail.className.replace("current", "");
            currentThumbnail = this;
            currentThumbnail.className += " current";

            image.src = this.getAttribute("src");
        });
    }

};